# Data Mining Project 1

We have developed and tested his project on a linux machine.

# WordClouds

The [word_cloud](https://github.com/amueller/word_cloud) library was used to generate the wordclouds
The library is pretty simple to use. We just need to pass a string of the words we want in our wordcloud
along with a set of stopwords and the library does the rest. In order to give more weight to the words in
the title of an entry we added the title multiple times in that string.
The stopwords set is composed of the wordcloud library's default stopword set, a small downloaded list of stopwords
and some words that were added to the above list by hand.