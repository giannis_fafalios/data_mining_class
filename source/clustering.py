import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from collections import Counter

from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS


train_data = fetch_20newsgroups(subset='train', shuffle=True)

stop_words = pd.read_csv('data/stopwords.csv', header=None)

counter = Counter()
for cat in train_data.target:
    counter[cat] += 1

for cat in set(train_data.target):
    print(train_data.target_names[cat] + " " + str(counter[cat]))

